package network;
import core.Jeu;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * Created by kenny on 10/05/2017.
 */
public class Serveur {

    public static void main(String[] args) throws RemoteException, MalformedURLException, InterruptedException {
        LocateRegistry.createRegistry(42008);
        System.setProperty("java.rmi.server.hostname", "127.0.0.1");
        System.setProperty("java.security.policy", "myfile.policy");
        System.setSecurityManager(new SecurityManager());
        IJeu server = new Jeu();
        Naming.rebind("rmi://127.0.0.1:42008/jeu", server);
        System.out.println("[System] Jeu is ready");

    }
}


