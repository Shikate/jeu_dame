package network;

import core.*;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Classe representant le gestionnaire de reseau de l'appli
 */
public interface IJeu extends Remote {


    /**
     * Methode qui permet d'initialiser le jeu (plateau et joueurs)
     * @throws RemoteException
     */
    public void initialisation() throws RemoteException;

    /**
     * Méthode de déplacement d'un jeton
     *  - Supprimer le jeton de l'ancienne case
     *  - Ajoute le jeton à la nouvelle case
     * @param j - Jeton
     * @param c_nouvelle - Ancienne case
     * @param c_ancienne - Nouvelle case
     */
    public void deplacerJetonCase(Jeton j,Case c_nouvelle,Case c_ancienne) throws RemoteException;

    /**
     * Méthode de suppression d'un pion
     *  - Supprimer le pion de sa case actuelle
     * @param pion
     * @return
     */
    public boolean supprimerPion(Jeton pion) throws RemoteException;

    /**
     * Méthode autorisation déplacement anger
     *  - vérifie que le joueur courrant peut manger un pion ou une dame.
     *
     * @return deplacement
     *  - Manger : si le joueur peut manger un jeton
     *  - Tous : si le joueur ne peux pas manger de jeton
     */
    public Deplacement autorisationDeplacementManger() throws RemoteException;

    /**
     * Méthode autorisation deplacement simple
     *  - vérifie si le joueur courrant peut au moins faire un deplacement de jeton simple
     *    cela permet de savoir si le joueur est bloquer ou non.
     * @return deplacement
     *  - Bloquer : si aucun déplacement simple est possible
     *  - Simple : si au moins un déplacement est possible
     */
    public Deplacement autorisationDeplacementSimple()throws RemoteException;


    /**
     * Méthode déplacer jeton
     *  - dispatch le déplacement d'un jeton selon les différents cas
     *  - vérifie le résultat du déplacement
     * @param j - Jeton à déplacer
     * @param c - Case du nouveau jeton
     * @throws RemoteException
     */
    public void deplacerJeton(Jeton j, Case c) throws RemoteException;

    /**
     * Méthode de gestion après un déplacement
     *  - incrémente le compteur si déplacement simple
     *  - vérifie si la partie est terminée ou non
     *  - switch de joueur si nécéssaire
     * @param j
     * @throws RemoteException
     */
    public void gestionApresDeplacer(Jeton j) throws RemoteException;

    /**
     * Méthode partie est terminée
     *  - Test si la partie est terminée selon les règles du jeu de dame
     *       1 - reste un jeton sur le plateau.
     *       2 - plus de 25 déplacement sans manger de jeton
     *       3 - (3dame) ou (2dame + 1pion) ou (1dame + 2 pion) et 16 déplacement simple
     *       4 - reste une dame pour chaque joueur
     *       5 - test si le joueur suivant n'est pas bloquer
     *
     * @return deplacement
     *  Bloquer : 5
     *  Egalier : 2,3,4
     *  Terminer : 1
     */
    public Deplacement partieEstTerminee() throws RemoteException;

    /**
     * Méthode de déplacement d'un pion simple
     * @param p - Pion
     * @param c - Case
     * @return deplacement
     *  Simple : si déplacement est possible
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerPionSimple(Pion p, Case c,boolean action_activer) throws RemoteException;

    /**
     * Méthode pion est une dame
     *  - Test après deplacement valide si le pion est une dame
     * @param p - Pion
     * @return boolean
     *  True: si le pion se situe dans la zone pour être une dame
     *  False : si le pion n'est pas dans la bonne zone pour devenir une dame
     */
    public boolean pionEstUneDame(Pion p) throws RemoteException;

    /**
     * Méthode de déplacement d'un pion manger
     * @param p - Pion
     * @param c - Case
     * @param activer_action : indique si l'action doit être réaliser
     * @return deplacement
     *  Manger : si déplacement est possible
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerPionManger(Pion p, Case c, boolean activer_action) throws RemoteException;

    /**
     * Méthode de déplacement d'une dame
     *  - vérifie que le déplacement d'une dame est valide
     *
     * @param d - Dame
     * @param c - Case
     * @param activer_action : indique si l'action doit être réaliser
     * @return deplacement
     *  Manger : si déplacement est possible est que la dame prend au moins un jeton
     *  Simple :  si e déplacement est valide sans prendre de pion
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerDame(Dame d, Case c,boolean activer_action) throws RemoteException;

    /**
     * Méthode de test si un pion peut manger
     *  - test si un pion peut manger un jeton dans les 4 positions possibles.
     *
     * @param p - Pion
     * @return deplacement
     *  Tous : si le pion ne peut pas manger
     *  Manger :  si au moins un déplacement du pion permet de manger un jeton
     */
    public Deplacement pionPeutManger(Pion p) throws RemoteException;

    /**
     * Méthode de test si une dame peut manger
     *  - test si un pion peut manger un jeton dans les 4 positions possibles.
     *
     * @param d - Dame
     * @return deplacement
     *  Tous : si la dame ne peut pas manger
     *  Manger :  si au moins un déplacement de la dame permet de manger un jeton
     */
    public Deplacement damePeutManger(Dame d,boolean deplacement_simple) throws RemoteException;

    /**
     * Methode qui permet de changer de joueur
     * @throws RemoteException
     */
    public void changerJoueur() throws RemoteException;

    /**
     * getPlateau
     * @return
     */
    public Plateau getPlateau() throws RemoteException;

    /**
     * Mehtode qui permet de retourner le joueur1
     * @return joueur1
     * @throws RemoteException
     */
    public Joueur getJoueur1() throws RemoteException;

    public void setJoueur1(Joueur joueur1) throws RemoteException;

    /**
     * Mehtode qui permet de retourner le joueur2
     * @return joueur2
     * @throws RemoteException
     */
    public Joueur getJoueur2() throws RemoteException;

    /**
     * setJoueur2
     * @param joueur2
     * @throws RemoteException
     */
    public void setJoueur2(Joueur joueur2) throws RemoteException;

    /**
     * getTour_Courrant
     * @return joueur_courrant
     */
    public Joueur getTour_courrant() throws RemoteException;

    /**
     * setTour_Courrant
     * @param tour_courrant
     */
    public void setTour_courrant(Joueur tour_courrant)throws RemoteException;


    /**
     * Methode qui retourne les jetons des deux joueurs sous forme d'une hashmap avec en indice le joueur
     * en valeur l'arraylist
     * @return
     */
    public HashMap<Joueur,ArrayList<Jeton>> getJetons() throws RemoteException;

    public String getName() throws RemoteException;

    public void send(String msg) throws RemoteException;

    /**
     * getDeplacement
     * @return deplacement
     */
    public Deplacement getDeplacement() throws RemoteException;



}
