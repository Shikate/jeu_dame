package network;

import core.Joueur;

import java.awt.*;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by kenny on 10/05/2017.
 */
public class Client {

    private Joueur joueur;

    public Client(){
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public IJeu connexionJoueur(String ipServeur, String nom) throws RemoteException, NotBoundException, MalformedURLException {
        System.setProperty("java.rmi.server.hostname", ipServeur);
        System.setProperty("java.security.policy", "myfile.policy");
        System.setSecurityManager(new SecurityManager());

        IJeu jeu = (IJeu) Naming.lookup("rmi://" + ipServeur + ":42008/jeu");

        System.out.println("connexion etablie");

        if (jeu.getJoueur1() == null) {
            jeu.setJoueur1(new Joueur(nom, Color.BLUE));
            this.joueur=jeu.getJoueur1();
            System.out.println("Vous etes le joueur 1");
        } else {
            jeu.setJoueur2(new Joueur(nom, Color.GREEN));
            this.joueur=jeu.getJoueur2();
            System.out.println("Vous etes le joueur 2");
        }

        if (jeu.getJoueur1() != null && jeu.getJoueur2() != null) {
            jeu.initialisation();

        }
        return jeu;

    }
}
