package core;


import network.IJeu;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import save.JeuDeserializer;

import java.awt.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteRef;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Classe representant une partie
 */
@JsonDeserialize(using = JeuDeserializer.class)
public class Jeu extends UnicastRemoteObject implements IJeu {

    private final long serialVersionUID = 42L;
    private String name;
    private Joueur joueur1;
    private Joueur joueur2;
    private Plateau plateau;
    private Joueur tour_courrant;
    private Deplacement deplacement;
    private Jeton jeton_courrant;
    private int compteur_deplacement_simple;

    /**
     * Constructeur jeu avec initialisation à null
     *
     * @throws RemoteException
     */
    public Jeu() throws RemoteException{
        this.plateau = new Plateau();
        this.joueur1 = null;
        this.joueur2 = null;
        this.tour_courrant = joueur1;
        this.compteur_deplacement_simple=0;
        this.jeton_courrant=null;
        this.deplacement = Deplacement.Tous;
    }

    /**
     *Constructeur Jeu avec 2 joueurs
     *
     * @param j1 - Joueur 1
     * @param j2 - Joueur 2
     * @throws RemoteException
     */
    public Jeu(Joueur j1, Joueur j2) throws RemoteException {
        this.plateau = new Plateau();
        this.joueur1 = j1;
        this.joueur2= j2;
        this.tour_courrant = j1;
        this.deplacement = Deplacement.Tous;
        this.compteur_deplacement_simple=0;
        this.jeton_courrant=null;
        this.deplacement = Deplacement.Tous;
    }

    //TODO Le contructeur est utilisé ?
    /**
     *
     * @param j1
     * @param j2
     * @param l_case
     * @param tourC
     * @throws RemoteException
     */
    public Jeu(Joueur j1, Joueur j2, ArrayList<Case> l_case, Joueur tourC) throws RemoteException{
        this.plateau = new Plateau();
        plateau.setListe_case(l_case);
        this.joueur1 = j1;
        this.joueur2= j2;
        this.tour_courrant = tourC;
        this.compteur_deplacement_simple=0;
        this.jeton_courrant=null;
        this.deplacement = Deplacement.Tous;
    }

    /**
     * Méthode d'initialisation d'un plateau de jeu de dame
     *  - Damier 10x10
     *
     * @throws RemoteException
     */
    public void initialisation() throws RemoteException{

        ArrayList<Case> liste_case = new ArrayList<Case>();
        int id=0;
        for (int i=9; i >=0 ; i--){
            for (int j=0; j < 10 ; j++){
                Case c = new Case(new Point(j,i),Color.BLACK);
                c.setJeton(null);
                if(i <= 3){
                    if((i%2==1 && j%2==1) || (i%2==0 && j%2==0)){
                        Jeton jeton = new Pion(joueur1.getCouleur(),id++);
                        c.setCouleur( Color.WHITE);
                        c.setJeton(jeton);
                    }
                }else if( i >= 6){
                    if((i%2==1 && j%2==1) || (i%2==0 && j%2==0)){
                        Jeton jeton = new Pion(joueur2.getCouleur(),id++);
                        c.setCouleur(Color.WHITE);
                        c.setJeton(jeton);
                    }
                }else if( (i%2==1 && j%2==1) || (i%2==0 && j%2==0) ){
                    c.setCouleur(Color.WHITE);
                }

                liste_case.add(c);
            }
        }
        this.plateau.setListe_case(liste_case);
    }


    /**
     * Méthode de déplacement d'un jeton
     *  - Supprimer le jeton de l'ancienne case
     *  - Ajoute le jeton à la nouvelle case
     * @param j - Jeton
     * @param c_nouvelle - Ancienne case
     * @param c_ancienne - Nouvelle case
     */
    public void deplacerJetonCase(Jeton j,Case c_nouvelle,Case c_ancienne) throws RemoteException {
        c_nouvelle.setJeton(j);
        c_ancienne.setJeton(null);
    }

    /**
     * Méthode de suppression d'un pion
     *  - Supprimer le pion de sa case actuelle
     * @param pion
     * @return
     */
    public boolean supprimerPion(Jeton pion) throws RemoteException {
        plateau.getCaseParJeton(pion).setJeton(null);
        return plateau.supprimerPion(pion);
    }

    /**
     * Méthode autorisation déplacement anger
     *  - vérifie que le joueur courrant peut manger un pion ou une dame.
     *
     * @return deplacement
     *  - Manger : si le joueur peut manger un jeton
     *  - Tous : si le joueur ne peux pas manger de jeton
     */
    public Deplacement autorisationDeplacementManger() throws RemoteException {
        Deplacement deplacement_tmp = Deplacement.Tous;
        if(Deplacement.Tous == deplacement ) {
            ArrayList<Jeton> list = getJetons().get(this.tour_courrant);
            for (Jeton jeton : list) {
                if(jeton instanceof Pion){
                    if(pionPeutManger((Pion)jeton) == Deplacement.Manger){
                        deplacement_tmp = Deplacement.Manger;
                        break;
                    }
                }else{
                    if(damePeutManger((Dame)jeton,false) == Deplacement.Manger){
                        deplacement_tmp = Deplacement.Manger;
                        break;
                    }
                }
            }
        }
        return deplacement_tmp;
    }

    /**
     * Méthode autorisation deplacement simple
     *  - vérifie si le joueur courrant peut au moins faire un deplacement de jeton simple
     *    cela permet de savoir si le joueur est bloquer ou non.
     * @return deplacement
     *  - Bloquer : si aucun déplacement simple est possible
     *  - Simple : si au moins un déplacement est possible
     */
    public Deplacement autorisationDeplacementSimple()throws RemoteException {

        Deplacement deplacement_tmp = Deplacement.Bloquer;

        ArrayList<Jeton> list = getJetons().get(this.tour_courrant);
        int y = this.joueur1 == this.tour_courrant ?  1 : -1 ;

        for (Jeton j : list) {
            Case c = plateau.getCaseParJeton(j);
            if( j instanceof Pion){
                int x_plus = (int)c.getPosition().getX()+1;
                int x_moins = (int)c.getPosition().getX()-1;
                int ycase = (int)c.getPosition().getY()+y;
                if(ycase >= 0 && ycase <=9 && x_moins>=0 && deplacerPionSimple((Pion) j, plateau.getCaseParCoordonees(x_moins, ycase), false) == Deplacement.Simple){
                    deplacement_tmp = Deplacement.Simple;
                    break;
                } else if(ycase >= 0 && ycase <=9 && x_plus<=9) {
                    if (deplacerPionSimple((Pion) j, plateau.getCaseParCoordonees(x_plus, ycase), false) == Deplacement.Simple) {
                        deplacement_tmp = Deplacement.Simple;
                        break;
                    }
                }
            }
            if(j instanceof Dame){
                for( Case case_tmp : plateau.getListe_case()){
                    if (case_tmp.estCasePionVide() && damePeutManger((Dame)j,true) == Deplacement.Simple ){
                        deplacement_tmp = Deplacement.Simple;
                        break;
                    }
                }
            }
        }
        return deplacement_tmp;
    }

    /**
     * Méthode déplacer jeton
     *  - dispatch le déplacement d'un jeton selon les différents cas
     *  - vérifie le résultat du déplacement
     * @param j - Jeton à déplacer
     * @param c - Case du nouveau jeton
     * @throws RemoteException
     */
    public void deplacerJeton(Jeton j, Case c) throws RemoteException{

        c=plateau.getCaseParCoordonees((int)c.getPosition().getX(),(int)c.getPosition().getY());

        //Test si le jeton peut faire plusieur prises consécutifs
        Deplacement deplacement_tmp = (this.deplacement == Deplacement.Manger ? Deplacement.Manger : autorisationDeplacementManger());
        System.out.println("1:"+deplacement_tmp);

        //Si la case est autorisée le jeton appartient au joueur courrant
        if(c.estCasePionVide() && this.tour_courrant.estMonJeton(j)) {
            switch (deplacement_tmp) {
                case Tous:
                case Impossible:
                case Simple:
                    if (j instanceof Pion) {
                        deplacement_tmp = deplacerPionSimple((Pion) j, c, true);
                        if (deplacement_tmp == Deplacement.Impossible) {
                            deplacement_tmp = deplacerPionManger((Pion) j, c, true);
                        }
                    } else {
                        deplacement_tmp = deplacerDame((Dame) j, c, true);
                    }
                    break;
                case Manger:
                    if (jeton_courrant == null || j == jeton_courrant) {
                        if (j instanceof  Pion ){
                            deplacement_tmp  = deplacerPionManger((Pion) j, c, true);
                            if(deplacement_tmp == deplacement.Manger){
                                jeton_courrant=j;
                            }
                        }else if (j instanceof  Dame){
                            if(deplacerDame((Dame) j, c, false) == Deplacement.Manger){
                                deplacement_tmp = deplacerDame((Dame) j, c, true);

                                if(deplacement_tmp == deplacement.Manger){
                                    jeton_courrant=j;
                                }
                            }
                        }
                    }else{
                        deplacement_tmp = Deplacement.Impossible;
                    }
                    break;
            }
            System.out.println(deplacement_tmp);
            if (deplacement_tmp != Deplacement.Impossible){
                this.deplacement=deplacement_tmp;
                gestionApresDeplacer(j);
            }
        }

    }

    /**
     * Méthode de gestion après un déplacement
     *  - incrémente le compteur si déplacement simple
     *  - vérifie si la partie est terminée ou non
     *  - switch de joueur si nécéssaire
     * @param j
     * @throws RemoteException
     */
    public void gestionApresDeplacer(Jeton j) throws RemoteException {
        //Si déplacement OK
        if (deplacement != Deplacement.Impossible) {

            //Si déplacement simple incrémentation du compteur
            if (deplacement == Deplacement.Simple) this.compteur_deplacement_simple++;
            else if (deplacement != Deplacement.Tous) this.compteur_deplacement_simple = 0;

            //Test partie terminer ou bloquer
            this.deplacement = partieEstTerminee();

            //Si partie terminer
            if (deplacement == Deplacement.Terminer || deplacement == Deplacement.Bloquer || deplacement == Deplacement.Egalite) {
                //Condition d'arrêt pour le jeu Deplacement = Terminer ou Egalite ou Bloquer
                //Joueur gagnant = joueur_courrant
                //this.deplacement = deplacement_tmp;
            } else {
                //Test si le pion devient une dame
                if (j instanceof Pion) pionEstUneDame((Pion) j);
                //Si déplacement manger on vérifie si le pion peut encore manger un autre pion
                if (deplacement == Deplacement.Manger) {
                    deplacement = (j instanceof Pion ? pionPeutManger((Pion) j) : damePeutManger((Dame) j,false));
                    jeton_courrant = deplacement == Deplacement.Manger ? j : null;
                }
                //Si déplacement différent de manger (changement de joueur)
                if (deplacement != Deplacement.Manger && deplacement != Deplacement.Impossible) {
                    try {
                        changerJoueur();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    jeton_courrant = null;
                    this.deplacement = Deplacement.Tous;
                }
            }
        //Si déplacmenent impossible, deplacement autorise = Tous et pas de changement de joueur
        } else {
            deplacement = Deplacement.Tous;
            jeton_courrant = null;
        }
    }

    /**
     * Méthode partie est terminée
     *  - Test si la partie est terminée selon les règles du jeu de dame
     *       1 - reste un jeton sur le plateau.
     *       2 - plus de 25 déplacement sans manger de jeton
     *       3 - (3dame) ou (2dame + 1pion) ou (1dame + 2 pion) et 16 déplacement simple
     *       4 - reste une dame pour chaque joueur
     *       5 - test si le joueur suivant n'est pas bloquer
     *
     * @return deplacement
     *  Bloquer : 5
     *  Egalier : 2,3,4
     *  Terminer : 1
     */
    public Deplacement partieEstTerminee() throws RemoteException {

        Deplacement deplacement_tmp = this.deplacement;

        int nb_dame_plateau = plateau.getNombreDame();
        int nb_pion_plateau = plateau.getNombrePion();

        //Si il reste aucun jeton adverse sur le plateau
        //Couleur du joueur adverse
        Color color = (tour_courrant == joueur1 ? joueur2.getCouleur() : joueur1.getCouleur());
        if(plateau.getNombreJeton(color) == 0 ){
            deplacement_tmp = Deplacement.Terminer;
        }

        //Si plus de 25 déplacement sans manger de jeton
        else if(this.compteur_deplacement_simple >= 25){
            deplacement_tmp = Deplacement.Egalite;
        }

        //Si (3dame) ou (2dame + 1pion) ou (1dame + 2 pion) et 16 déplacement simple
        else if( this.compteur_deplacement_simple >= 16 && nb_dame_plateau != 0 && (nb_dame_plateau + nb_pion_plateau) == 3){
            deplacement_tmp = Deplacement.Egalite;

        //Si il reste une dame pour chaque joueur
        }else if ( nb_dame_plateau == 2 && nb_pion_plateau == 0){
            deplacement_tmp = Deplacement.Egalite;
        }

        //Switch de joueur pour vérifier si le tour suivant le joueur sera bloquer ou non.
        this.changerJoueur();

        if( deplacement_tmp != Deplacement.Egalite && deplacement_tmp != Deplacement.Terminer && autorisationDeplacementManger() == Deplacement.Tous && autorisationDeplacementSimple() == Deplacement.Bloquer){
            deplacement_tmp = Deplacement.Bloquer;
        }
        this.changerJoueur();


        return deplacement_tmp;
    }

    /**
     * Méthode de déplacement d'un pion simple
     * @param p - Pion
     * @param c - Case
     * @return deplacement
     *  Simple : si déplacement est possible
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerPionSimple(Pion p, Case c,boolean action_activer) throws RemoteException {
        //Deplacement simple du jouer, calcul du déplacement selon j1 ou j2
        int y = this.joueur1 == this.tour_courrant ?  1 : -1 ;
        //Deplacement simple du joueur
        if( c.estCasePionVide() &&  plateau.getCaseParJeton(p).getPosition().getY() + y == c.getPosition().getY() &&
                Math.abs(plateau.getCaseParJeton(p).getPosition().getX() - c.getPosition().getX()) == 1){

            if (action_activer) {
                deplacerJetonCase((Jeton)p,c,plateau.getCaseParJeton(p));
            }
            return Deplacement.Simple;
        }
        return Deplacement.Impossible;
    }

    /**
     * Méthode pion est une dame
     *  - Test après deplacement valide si le pion est une dame
     * @param p - Pion
     * @return boolean
     *  True: si le pion se situe dans la zone pour être une dame
     *  False : si le pion n'est pas dans la bonne zone pour devenir une dame
     */
    public boolean pionEstUneDame(Pion p) throws RemoteException {
        int y = p.getCouleur().equals(joueur1.getCouleur()) ? 9 : 0;
        if (this.plateau.getCaseParJeton(p).getPosition().getY() == y){
            Case c = plateau.getCaseParJeton(p);
            Dame d = new Dame(p.getCouleur(),p.getId());
            c.setJeton(d);
            p.setEst_en_jeu(false);
            return true;
        }
        return false;
    }

    /**
     * Méthode de déplacement d'un pion manger
     * @param p - Pion
     * @param c - Case
     * @param activer_action : indique si l'action doit être réaliser
     * @return deplacement
     *  Manger : si déplacement est possible
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerPionManger(Pion p, Case c, boolean activer_action) throws RemoteException {

        Deplacement deplacement = Deplacement.Impossible;

        Color couleur_manger = this.joueur1 == this.tour_courrant ?  joueur2.getCouleur() : joueur1.getCouleur() ;

        if( c.estCasePionVide() && Math.abs(plateau.getCaseParJeton(p).getPosition().getX() - c.getPosition().getX()) == 2 &&
                (Math.abs(plateau.getCaseParJeton(p).getPosition().getY() - c.getPosition().getY()) == 2) ){

            int x_milieu = plateau.getCaseParJeton(p).getPosition().getX() < c.getPosition().getX() ? 1 : -1;
            int y_milieu = plateau.getCaseParJeton(p).getPosition().getY() < c.getPosition().getY() ? 1 : -1;

            Case case_milieu = plateau.getCaseParCoordonees((int)plateau.getCaseParJeton(p).getPosition().getX() + x_milieu , (int)plateau.getCaseParJeton(p).getPosition().getY() + y_milieu);
            if( ! case_milieu.estVide() && case_milieu.getJeton().getCouleur().equals(couleur_manger) ){

                if(activer_action) {
                    case_milieu.setJeton(null);
                    deplacerJetonCase(p, c, plateau.getCaseParJeton(p));
                }

                deplacement = Deplacement.Manger;
            }
        }
        return  deplacement;
    }


    /**
     * Méthode de déplacement d'une dame
     *  - vérifie que le déplacement d'une dame est valide
     *
     * @param d - Dame
     * @param c - Case
     * @param activer_action : indique si l'action doit être réaliser
     * @return deplacement
     *  Manger : si déplacement est possible est que la dame prend au moins un jeton
     *  Simple :  si e déplacement est valide sans prendre de pion
     *  Impossible : si le déplacement ne répond pas aux critères
     */
    public Deplacement deplacerDame(Dame d, Case c,boolean activer_action) throws RemoteException {
        Deplacement deplacement = Deplacement.Impossible;
        Case cd= plateau.getCaseParJeton(d);
        int x_dame = (int)cd.getPosition().getX();
        int y_dame = (int)cd.getPosition().getY();
        int x_case = (int)c.getPosition().getX();
        int y_case = (int)c.getPosition().getY();
        boolean b=false;boolean t=false;
        ArrayList<Case> ac=new ArrayList<Case>();
        int x=(x_case>x_dame?1:-1);
        int y=(y_case>y_dame?1:-1);
        int x2 = x;
        int y2 = y;
        //si deplacement diag
        if( Math.abs(x_dame-x_case) == Math.abs(y_dame-y_case ) && c.estCasePionVide()){
            while(x_dame+x!=x_case && y_dame+y!= y_case && !t && x+x_dame >= 0 && x+x_dame <= 9  && y+y_dame >= 0 && y+y_dame <= 9) {
                //procedure de desactivation du deplacement si jeton allie ou deux jetons adverses
                Case c2 =plateau.getCaseParCoordonees(x_dame+x,y_dame+y);
                if(c2.getJeton()!= null){
                    if(b){
                        //Si 2 pions successif
                        t=true;
                    }else{
                        if(!c2.getJeton().getCouleur().equals(d.getCouleur())){
                            ac.add(c2);
                        }else{
                            //Si pion de la même couleur
                            t=true;
                        }
                        b=true;
                    }
                }else{
                    b=false;
                }
                x=x+x2;
                y=y+y2;
            }

            //si le deplacement n'a pas ete desactive ci-dessus
            if(!t){
                deplacement  =  (ac.size() > 0 ? Deplacement.Manger : Deplacement.Simple) ;
                for(Case c_tmp : ac){
                    if(activer_action) {
                        supprimerPion(c_tmp.getJeton());
                    }
                }
                if(activer_action) deplacerJetonCase(d,c,plateau.getCaseParJeton(d));
            }
        }
        //retour booleen du deplacement
        return deplacement;
    }

    /**
     * Méthode de test si un pion peut manger
     *  - test si un pion peut manger un jeton dans les 4 positions possibles.
     *
     * @param p - Pion
     * @return deplacement
     *  Tous : si le pion ne peut pas manger
     *  Manger :  si au moins un déplacement du pion permet de manger un jeton
     */
    public Deplacement pionPeutManger(Pion p) throws RemoteException {

        int x = (int) plateau.getCaseParJeton(p).getPosition().getX();
        int y = (int) plateau.getCaseParJeton(p).getPosition().getY();

        Case c1 = plateau.getCaseParCoordonees(x+2,y+2);
        Case c2 = plateau.getCaseParCoordonees(x+2,y-2);
        Case c3 = plateau.getCaseParCoordonees(x-2,y+2);
        Case c4 = plateau.getCaseParCoordonees(x-2,y-2);

        if (    (c1 != null && deplacerPionManger(p,c1,false) == Deplacement.Manger) ||
                (c2 != null && deplacerPionManger(p,c2,false) == Deplacement.Manger) ||
                (c3 != null && deplacerPionManger(p,c3,false) == Deplacement.Manger) ||
                (c4 != null && deplacerPionManger(p,c4,false) == Deplacement.Manger) ){
            return Deplacement.Manger;
        }else{
            return Deplacement.Tous;
        }
    }

    /**
     * Méthode de test si une dame peut manger
     *  - test si un pion peut manger un jeton dans les 4 positions possibles.
     *
     * @param d - Dame
     * @return deplacement
     *  Tous : si la dame ne peut pas manger
     *  Manger :  si au moins un déplacement de la dame permet de manger un jeton
     */
    public Deplacement damePeutManger(Dame d,boolean deplacement_simple) throws RemoteException {

        Deplacement deplacement_tmp= Deplacement.Tous;

        int x = (int) plateau.getCaseParJeton(d).getPosition().getX();
        int y = (int) plateau.getCaseParJeton(d).getPosition().getY();

        int i = x;int j = y;

        while ( i<= 9  && j <= 9 ){
            if( (!deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Manger) ||
                    (deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Simple) ){
                deplacement_tmp =  deplacement_simple ? Deplacement.Simple : Deplacement.Manger;
                break;
            }
            i++;j++;
        }

        i=x;j=y;
        while ( i <= 9  && j >= 0 ){
            if( (!deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Manger) ||
                    (deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Simple) ){
                deplacement_tmp =  deplacement_simple ? Deplacement.Simple : Deplacement.Manger;
                break;
            }
            i++;j--;
        }
        i=x;j=y;
        while ( i >= 0  & j <= 9 ){
            if( (!deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Manger) ||
                    (deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Simple) ){
                deplacement_tmp =  deplacement_simple ? Deplacement.Simple : Deplacement.Manger;
                break;
            }
            i--;j++;
        }
        i=x;j=y;
        while ( i >= 0  && j >= 0 ){
            if( (!deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Manger) ||
                    (deplacement_simple && deplacerDame(d,plateau.getCaseParCoordonees(i,j),false) == Deplacement.Simple) ){
                deplacement_tmp =  deplacement_simple ? Deplacement.Simple : Deplacement.Manger;
                break;
            }
            i--;j--;
        }

        if(deplacement_tmp == Deplacement.Manger && deplacement_simple) deplacement_tmp = Deplacement.Tous;
        else if( deplacement_tmp == Deplacement.Simple && ! deplacement_simple) deplacement_tmp = Deplacement.Tous;
        return deplacement_tmp;
    }


    /**
     * Méthode de changement de joueur
     *  - switch le joueur courrant
     * @throws RemoteException
     */
    public void changerJoueur() throws RemoteException {
        if (this.joueur1 == this.tour_courrant) this.tour_courrant = this.joueur2;
        else this.tour_courrant = this.joueur1;
    }

    /**
     * getPlateau
     * @return
     */
    public Plateau getPlateau() throws RemoteException {
        return plateau;
    }

    /**
     * getJoueur1
     * @return joueur1
     * @throws RemoteException
     */
    public Joueur getJoueur1() throws RemoteException {
        return joueur1;
    }

    /**
     * setJoueur1
     * @param joueur1
     * @throws RemoteException
     */
    public void setJoueur1(Joueur joueur1) throws RemoteException {
        this.joueur1 = joueur1;
        if(tour_courrant == null){
            this.setTour_courrant(joueur1);
        }
    }

    /**
     * getJoueur2
     * @return joueur2
     * @throws RemoteException
     */
    public Joueur getJoueur2() throws RemoteException{
        return joueur2;
    }

    /**
     * setJoueur2
     * @param joueur2
     * @throws RemoteException
     */
    public void setJoueur2(Joueur joueur2) throws RemoteException {
        this.joueur2 = joueur2;
        if(tour_courrant == null){
            this.setTour_courrant(joueur2);
        }
    }

    /**
     * getTour_Courrant
     * @return joueur_courrant
     */
    public Joueur getTour_courrant() throws RemoteException {
        return tour_courrant;
    }

    /**
     * setTour_Courrant
     * @param tour_courrant
     */
    public void setTour_courrant(Joueur tour_courrant)throws RemoteException {
        this.tour_courrant = tour_courrant;
    }

    /**
     * ToString
     * @return string
     */
    public String toString() {
        StringBuffer sb=new StringBuffer();
        for(int i=9; i>=0; i--){
            for(int j=0; j<=9; j++){
                if(this.plateau.getCaseParCoordonees(j,i) !=null)
                sb.append(this.plateau.getCaseParCoordonees(j,i).toString());
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Methode qui retourne les jetons des deux joueurs sous forme d'une hashmap avec en indice le joueur
     * en valeur l'arraylist
     * @return
     */
    public HashMap<Joueur,ArrayList<Jeton>> getJetons() throws RemoteException{
        ArrayList<Jeton> l1= new ArrayList<Jeton>();
        ArrayList<Jeton> l2= new ArrayList<Jeton>();

        for(Case c :this.getPlateau().getListe_case()){
            if(c.getJeton()!=null){
                //si un jeton est present on determine son possesseur
                if(c.getJeton().getCouleur().equals(joueur1.getCouleur())){
                    l1.add(c.getJeton());
                }else{
                    l2.add(c.getJeton());
                }
            }
        }

        HashMap<Joueur,ArrayList<Jeton>> tabListJetons=new HashMap<Joueur,ArrayList<Jeton>>();
        tabListJetons.put(joueur1, l1);
        tabListJetons.put(joueur2, l2);
        return tabListJetons;
    }

    /**
     * getName
     * @return
     * @throws RemoteException
     */
    public String getName() throws RemoteException {
        return this.name;
    }

    /**
     * send
     * @param msg
     * @throws RemoteException
     */
    public void send(String msg) throws RemoteException {
        System.out.println(msg);
    }

    /**
     * getDeplacement
     * @return deplacement
     */
    public Deplacement getDeplacement() throws RemoteException {
        return deplacement;
    }

    /**
     * getCompteur_deplacement_simple
     * @return compteur_deplacement_simple
     */
    public int getCompteur_deplacement_simple() {
        return compteur_deplacement_simple;
    }

    /**
     * setCompteur_deplacement_simple
     * @param compteur_deplacement_simple
     */
    public void setCompteur_deplacement_simple(int compteur_deplacement_simple) {
        this.compteur_deplacement_simple = compteur_deplacement_simple;
    }


    @Override
    @JsonIgnore
    public RemoteRef getRef() {
        return super.getRef();
    }

    public void setJeton_courrant(Jeton jeton_courrant) {
        this.jeton_courrant = jeton_courrant;
    }
}
