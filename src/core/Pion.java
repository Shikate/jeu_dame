package core;

import java.awt.*;

/**
 * Classe representant un pion du jeu de dame
 */
public class Pion extends Jeton{


    /**
     * Constructeur de pion
     * @param c la couleur du pion
     * @param id l'id unique du pion sur le plateau
     */
    public Pion(Color c,int id){

        super(c,id);
    }

    /**
     * Constructeur de pion a partir d'une couleur int
     * @param c le code int de la couleur
     * @param id l'id unique du pion sur le plateau
     */
    public Pion(int c, int id){

        super(c,id);
    }


}
