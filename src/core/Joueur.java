package core;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import save.JoueurSerializer;

import java.awt.*;
import java.io.Serializable;

/**
 * Classe representant un joueur
 */
@JsonSerialize(using = JoueurSerializer.class)
public class Joueur implements Serializable {
    final long serialVersionUID = 42L;
    /**
     * Attribut donnant le nom du joueur
     */
    private String nom;
    /**
     * Attribut donnant la couleur du joueur
     */
    private Color couleur;


    /**
     * Constructeur de joueur
     * @param n nom du joueur
     * @param c couleur du joeuur
     */
    public Joueur(String n,Color c){
        this.nom=n;
        this.couleur=c;
    }
    /**
     * Constructeur de joueur
     * @param n nom du joueur
     * @param c couleur du joeuur
     */
    public Joueur(String n,int c){
        this.nom=n;
        this.couleur= new Color(c);
    }

    /**
     * Fonction permettant de savoir si le jetonn appartient au joueur
     * @param jeton
     * @return
     */
    public boolean estMonJeton(Jeton jeton){
        return jeton.getCouleur().equals(couleur);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

}
