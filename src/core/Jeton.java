package core;


import org.codehaus.jackson.map.annotate.JsonSerialize;
import save.JetonSerializer;

import java.awt.*;
import java.io.Serializable;

/**
 * Classe representant un jeton du jeu de dame
 */
@JsonSerialize(using = JetonSerializer.class)
public abstract class Jeton implements Serializable {
    /**
     * Attribut long correspondant a la serial version
     */
    final long serialVersionUID = 42L;
    /**
     * Attribut color correspondant a la couleur du jeton
     */
    protected Color couleur;
    /**
     * Attribut boolean correspondant au fait qu'un jeton est en jeu - vaut true si jeton en jeu
     */
    protected boolean est_en_jeu;
    /**
     * Attribut int correspondant a l'id unique du jeton sur le plateau
     */
    protected int id;

    /**
     * Constructeur du jeton
     * @param couleur color du jeton
     * @param id l'id unique du jeton sur le plateau
     */
    public Jeton(Color couleur,int id) {
        this.couleur = couleur;
        this.id = id;
        this.est_en_jeu=true;

    }


    /**
     * Constructeur de jeton a l'aide d'une couleur int
     * @param couleur le code couleur du jeton
     * @param id l'id unique du jeton sur le plateau
     */
    public Jeton(int couleur,int id) {
        this.couleur = new Color(couleur);
        this.id=id;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public int getId() {
        return id;
    }


    public boolean isEst_en_jeu() {

        return est_en_jeu;
    }

    public void setEst_en_jeu(boolean est_en_jeu) {
        this.est_en_jeu = est_en_jeu;
    }


}


