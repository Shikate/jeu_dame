package core;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 * Classe representant un plateau du jeu
 */
public class Plateau implements Serializable {


    /**
     * Attribut donnant le nombre de casse dans le jeu
     */
    public final static int NB_CASE=100;
    final long serialVersionUID = 42L;
    /**
     * Attribut donnant la liste de case du plateau
     */
    private ArrayList<Case> liste_case;

    /**
     * Constucteur de plateau
     */
    public Plateau(){
        this.liste_case = new ArrayList<Case>();
    }

    /**
     * Fonction donnant le nombre de case en jeu
     * @return
     */
    public static int getNbCase() {
        return NB_CASE;
    }

    public ArrayList<Case> getListe_case() {
        return liste_case;
    }

    public void setListe_case(ArrayList<Case> liste_case) {
        this.liste_case = liste_case;
    }

    /**
     * Fonction de suppression d'un pion
     * @param pion
     * @return vrai si supprimer
     */
    public boolean supprimerPion(Jeton pion){
        boolean topPionSupprimer = false;
        for (Case c: liste_case) {
            if(c.getJeton() instanceof  Pion && pion.equals(c.getJeton())){
                c.setJeton(null);
                pion.setEst_en_jeu(false);
                topPionSupprimer = true;
            }
        }
        return topPionSupprimer;
    }


    public Case getCaseParCoordonees(int x, int y){
        for(Iterator<Case> i=liste_case.iterator(); i.hasNext();){
            Case c = i.next();
            if(c.getPosition().getX()==x && c.getPosition().getY()==y){
                return c;
            }
        }
        return null;
    }
    
    public Case getCaseParJeton(Jeton j){
        for(Case c : this.getListe_case()) {
            if (c.getJeton()!=null && c.getJeton().getId() == j.getId()) {
                return c;
            }
        }
        return null;
    }

    public int getNombreDame(){
        return liste_case.stream().filter(c -> c.getJeton() != null && c.getJeton() instanceof Dame)
                .collect(Collectors.toList()).size();
    }

    public int getNombrePion(){
        return liste_case.stream().filter(c -> c.getJeton() != null && c.getJeton() instanceof Pion)
                .collect(Collectors.toList()).size();
    }

    public int getNombreJeton(Color color){
        return liste_case.stream().filter(c -> c.getJeton() != null && c.getJeton().getCouleur().equals(color))
                .collect(Collectors.toList()).size();
    }

    /**
     * fonction qui supprime tous les pions
     */
    public void supprimerTousLesPions(){
        for( Case c : getListe_case()){
            if(c.getJeton() != null){
                supprimerPion(c.getJeton());
            }
        }
    }

}
