package core;

/**
 * Created by beurk on 12/05/2017.
 */
public enum Deplacement {

    Tous,
    Simple,
    Manger,
    Impossible,
    Terminer,
    Egalite,
    Bloquer,

}
