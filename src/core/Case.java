package core;

import javafx.scene.layout.StackPane;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import save.CaseSerializer;

import ui.Panneau;

import java.awt.*;
import java.io.Serializable;
import java.awt.Color;

/**
 * Classe representant une case du plateau
 */
@JsonSerialize(using = CaseSerializer.class)
public class Case extends StackPane implements Serializable{
    /**
     * Attribut long correspondant a la serial version
     */
    private final long serialVersionUID = 42L;
    /**
     * Attribut point correspondant a la position de la case sur le plateau
     */
    protected Point position;

    /**
     * Attribut jeton correspondant au jeton contenu dans la case
     */
    protected Jeton jeton;

    /**
     * Attribut color correspondant a la couleur de la casse
     */
    protected Color couleur;

    /**
     * Constructeur de Case
     * @param p le point position de la case sur le plateau
     * @param c la couleur de la case
     */
    public Case(Point p,Color c){
        this.position=p;
        this.couleur=c;
        dessinerCase();
    }



    public Point getPosition() {

        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
        Shape s = (Shape) getChildren().get(0);
        s.setFill(javafx.scene.paint.Color.rgb(couleur.getRed(),couleur.getGreen(),couleur.getBlue()));
    }

    /**
     * Methode permettant de savoir si la case est une case pouvant accueillir un pion
     * @return true si la case peut accueillir un pion
     */
    public boolean estCasePion(){
        return this.couleur == Color.WHITE;
    }

    /**
     * Methode permettant de definir si la case est une case pouvant accueillir un pion et qui est vide
     * @return true si la case peut accueillir un pion et est vice
     */
    public boolean estCasePionVide(){
        return this.couleur.equals(Color.WHITE) && estVide();
    }

    public Jeton getJeton()
    {
        return jeton;
    }

    public void setJeton(Jeton jeton)
    {
        this.jeton = jeton;
        dessinerCase();
    }

    /**
     * Methode permettant de savoir si la case est vide
     * @return true si la case est vice
     */
    public boolean estVide(){
        if(this.jeton==null){
            return true;
        }
        return false;
    }

    /**
     * Methode permettant de dessiner une case
     */
    public void dessinerCase(){

        getChildren().clear();

        relocate(position.getX() * Panneau.CASE_SIZE,Math.abs(position.getY()-9) * Panneau.CASE_SIZE);

        Rectangle rectangle = new Rectangle(position.getX() * Panneau.CASE_SIZE,position.getY() * Panneau.CASE_SIZE, Panneau.CASE_SIZE, Panneau.CASE_SIZE);
        rectangle.setFill(javafx.scene.paint.Color.rgb(couleur.getRed(),couleur.getGreen(),couleur.getBlue()));

        getChildren().add(0,rectangle);

        if(!this.estVide()){
            dessinerJeton();
        }

    }

    /**
     * Methode permettant de dessiner un jeton
     */
    private void dessinerJeton(){

        Ellipse ellipse = new Ellipse(Panneau.CASE_SIZE/2,Panneau.CASE_SIZE/2,Panneau.CASE_SIZE * 0.25, Panneau.CASE_SIZE * 0.25);
        ellipse.setFill(javafx.scene.paint.Color.rgb(jeton.getCouleur().getRed(),jeton.getCouleur().getGreen(),jeton.getCouleur().getBlue()));

        ellipse.setStroke(javafx.scene.paint.Color.BLACK);
        ellipse.setStrokeWidth(Panneau.CASE_SIZE * 0.03);

        getChildren().add(1,ellipse);

        if(this.getJeton() instanceof Dame){
            Ellipse ellipseDame = new Ellipse(Panneau.CASE_SIZE/2,Panneau.CASE_SIZE/2,Panneau.CASE_SIZE / 8, Panneau.CASE_SIZE / 8);
            ellipseDame.setFill(javafx.scene.paint.Color.BLACK);
            getChildren().add(2,ellipseDame);
        }
    }

    /**
     * Methode permettant d'afficher la selection de la case
     */
    public void selectCase(){
        if(!this.estVide()){
            Ellipse el=(Ellipse) getChildren().get(1);
            el.setFill(javafx.scene.paint.Color.RED);
        }else{
            Rectangle rectangle=(Rectangle) getChildren().get(0);
            rectangle.setFill(javafx.scene.paint.Color.YELLOW);
        }
    }

    /**
     * Methode permettant d'afficher la deselection de la case
     */
    public void unSelectCase(){
        System.out.println("deseltion de "+position);
        dessinerCase();
    }

}
