package core;

import java.awt.*;

/**
 * Classe representant une dame, pion particulier du jeu de dame
 */
public class Dame extends Jeton{

    /**
     * Constructeur de dame
     * @param c  la couleur de la dame
     * @param id l'id unique de la dame sur le plateau
     */
    public Dame(Color c, int id)
    {
        super(c,id);
    }

    /**
     * Constructeur de dame a partir du code couleur
     * @param c code couleur
     * @param id l'id unique de la dame sur le plateau
     */
    public Dame(int c, int id)
    {
        super(c, id);
    }


}
