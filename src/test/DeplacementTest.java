/**
 * Classe de tests du package core - Deplacement
 */



import core.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.ArrayList;

@RunWith(JUnitPlatform.class)

public class DeplacementTest {

    private Jeu jeu;
    private Joueur j1;
    private Joueur j2;
    private ArrayList<Jeton> listeJetonsJ1;
    private ArrayList<Jeton> listeJetonsJ2;
    private Plateau plateau;


    @Before
    public void initBefore() throws RemoteException {

        j1=new Joueur("Hugues",Color.red);
        j2=new Joueur("Jacques",Color.green);

        jeu=new Jeu(j1,j2);
        jeu.initialisation();

        listeJetonsJ1=jeu.getJetons().get(j1);
        listeJetonsJ2=jeu.getJetons().get(j2);
        plateau=jeu.getPlateau();


        //Suppression d'es deux premières lignes de chaque joueurs
        jeu.supprimerPion(plateau.getCaseParCoordonees(0,0).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(2,0).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(4,0).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(6,0).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(8,0).getJeton());

        jeu.supprimerPion(plateau.getCaseParCoordonees(1,1).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(3,1).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(5,1).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(7,1).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(9,1).getJeton());

        jeu.supprimerPion(plateau.getCaseParCoordonees(0,8).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(2,8).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(4,8).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(6,8).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(8,8).getJeton());

        jeu.supprimerPion(plateau.getCaseParCoordonees(1,9).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(3,9).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(5,9).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(7,9).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(9,9).getJeton());

        jeu.supprimerPion(plateau.getCaseParCoordonees(0,6).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(1,7).getJeton());
        jeu.supprimerPion(plateau.getCaseParCoordonees(7,7).getJeton());

        Jeton j = plateau.getCaseParCoordonees(0,2).getJeton();

        jeu.deplacerJetonCase(j,plateau.getCaseParCoordonees(1,9),plateau.getCaseParJeton(j));

        j = plateau.getCaseParCoordonees(7,3).getJeton();
        jeu.deplacerJetonCase(j,plateau.getCaseParCoordonees(7,5),plateau.getCaseParJeton(j));

        j = plateau.getCaseParCoordonees(2,2).getJeton();
        jeu.deplacerJetonCase(j,plateau.getCaseParCoordonees(6,8),plateau.getCaseParJeton(j));

    }

    @Test
    public void testDeplacementPionSimpleDroite() throws RemoteException {
        jeu.setTour_courrant(j1);
        System.out.println(jeu);
        Pion p = (Pion)plateau.getCaseParCoordonees(5,3).getJeton();
        Case c = plateau.getCaseParCoordonees(6,4);
        jeu.deplacerPionSimple(p,c,true);
        System.out.println(jeu);
        Assert.assertEquals(c.getPosition(),plateau.getCaseParJeton(p).getPosition());
    }

    @Test
    public void testDeplacementPionSimpleGauche() throws RemoteException {
        jeu.setTour_courrant(j1);
        Pion p = (Pion)plateau.getCaseParCoordonees(5,3).getJeton();
        Case c = plateau.getCaseParCoordonees(4,4);
        jeu.deplacerPionSimple(p,c,true);
        Assert.assertEquals(c.getPosition(),plateau.getCaseParJeton(p).getPosition());
    }


    @Test
    public void testDeplacementCaseNonAutorise() throws RemoteException {
        /*
        * Deplacement horizontal
        */
        jeu.setTour_courrant(j2);

        Pion p = (Pion)plateau.getCaseParCoordonees(1,3).getJeton();
        Case c = plateau.getCaseParCoordonees(1,2);

        Assert.assertEquals(jeu.deplacerPionSimple(p,c,true),Deplacement.Impossible);


        /*
        * Deplacement vertical
        * */

        p = (Pion)plateau.getCaseParCoordonees(1,3).getJeton();
        c = plateau.getCaseParCoordonees(1,4);

        Assert.assertEquals(jeu.deplacerPionSimple(p,c,true),Deplacement.Impossible);

    }

    @Test
    public void testDeplacementPionAdversaire() throws RemoteException {
        jeu.setTour_courrant(j2);
        Pion p = (Pion)plateau.getCaseParCoordonees(5,3).getJeton();
        Case c = plateau.getCaseParCoordonees(4,4);
        Assert.assertEquals(jeu.deplacerPionSimple(p,c,true),Deplacement.Impossible);
    }

    @Test
    public void testDeplacementPionCaseNonVide() throws RemoteException {
        jeu.setTour_courrant(j2);
        Pion p = (Pion)plateau.getCaseParCoordonees(2,2).getJeton();
        Case c = plateau.getCaseParCoordonees(3,3);
        Assert.assertEquals(jeu.deplacerPionSimple(p,c,true),Deplacement.Impossible);
    }

    @Test
    public void testDeplacementPionMangePionAvant() throws RemoteException {
        jeu.setTour_courrant(j2);
        Pion p = (Pion)plateau.getCaseParCoordonees(6,6).getJeton();
        Case c = plateau.getCaseParCoordonees(8,4);

        jeu.deplacerPionManger(p,c,true);
        Assert.assertNull(plateau.getCaseParCoordonees(7,5).getJeton());
        Assert.assertEquals(plateau.getCaseParJeton(p).getPosition(),c.getPosition());

    }

    @Test
    public void testDeplacementPionMangePionArriere() throws RemoteException {

        jeu.setTour_courrant(j2);
        Pion p = (Pion)plateau.getCaseParCoordonees(5,7).getJeton();
        Case c = plateau.getCaseParCoordonees(7,9);

        jeu.deplacerPionManger(p,c,true);
        Assert.assertNull(plateau.getCaseParCoordonees(6,8).getJeton());
        Assert.assertEquals(plateau.getCaseParJeton(p).getPosition(),c.getPosition());

    }

    @Test
    public void testDeplacementPionEstDame() throws RemoteException {

        Pion p = (Pion) plateau.getCaseParCoordonees(1,9).getJeton();

        Assert.assertTrue(jeu.pionEstUneDame(p));

    }

    @Test
    public void testDeplacementDameSimple() throws RemoteException {

        Pion p = (Pion) plateau.getCaseParCoordonees(1,9).getJeton();
        Assert.assertTrue(jeu.pionEstUneDame(p));

        Dame dame =  (Dame)plateau.getCaseParCoordonees(1,9).getJeton();
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(2,8),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(2,8));

        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(0,6),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(0,6));

    }

    @Test
    public void testDeplacementDameManger() throws RemoteException {

        Pion p = (Pion) plateau.getCaseParCoordonees(1,9).getJeton();
        jeu.pionEstUneDame(p);
        Dame dame= (Dame)plateau.getCaseParCoordonees(1,9).getJeton();
        Assert.assertTrue(dame.getClass()==Dame.class);

        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(2,8),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(2,8));

        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(1,7),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(1,7));
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(3,5),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(3,5));

    }

    @Test
    public void testDeplacementDameAvance() throws RemoteException {

        Pion p = (Pion) plateau.getCaseParCoordonees(1,9).getJeton();
        Assert.assertTrue(jeu.pionEstUneDame(p));
        Dame dame= (Dame) plateau.getCaseParCoordonees(1,9).getJeton();

        //ok
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(2,8),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(2,8));

        //ok
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(1,7),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(1,7));

        //nok
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(7,9),true);
        Assert.assertNotSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(7,9));

        //ok avec mange
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(3,5),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(3,5));

        //ok
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(2,4),true);
        Assert.assertSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(2,4));

        //nok avec non mange
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(7,9),true);
        Assert.assertNotSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(7,9));

        //nok avec deplacement interdit par dessus sa piece
        jeu.deplacerDame(dame,plateau.getCaseParCoordonees(0,2),true);
        Assert.assertNotSame(plateau.getCaseParJeton(dame),plateau.getCaseParCoordonees(0,2));
    }

    @Test
    public void testDeplacementDameImpossible() throws RemoteException {
        Pion p = (Pion) plateau.getCaseParCoordonees(1,9).getJeton();
        jeu.pionEstUneDame(p);
        Dame dame = (Dame)plateau.getCaseParCoordonees(1,9).getJeton();
        Assert.assertTrue(dame.getClass()==Dame.class);
        Assert.assertEquals(jeu.deplacerDame(dame,plateau.getCaseParCoordonees(1,8),true),Deplacement.Impossible);
    }


}
