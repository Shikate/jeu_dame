/**
 * Classe de tests du package core - Deplacement
 */



import core.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.awt.*;
import java.rmi.RemoteException;

@RunWith(JUnitPlatform.class)

public class JeuTest {

    private Jeu jeu;
    private Joueur j1;
    private Joueur j2;
    private Plateau plateau;


    @Before
    public void initBefore() throws RemoteException {

        j1=new Joueur("Hugues",Color.red);
        j2=new Joueur("Jacques",Color.green);

        jeu=new Jeu(j1,j2);
        jeu.initialisation();

        plateau=jeu.getPlateau();

    }

    @Test
    public void testCreationInitialisation() throws RemoteException {

        Assert.assertEquals(jeu.getTour_courrant(),j1);

        //Joueur 1
        Assert.assertEquals(plateau.getCaseParCoordonees(0,0).getJeton().getCouleur(),j1.getCouleur());
        Assert.assertEquals(plateau.getCaseParCoordonees(0,1).getJeton(),null);

        //Joueur 2
        Assert.assertEquals(plateau.getCaseParCoordonees(9,9).getJeton().getCouleur(),j2.getCouleur());
        Assert.assertEquals(plateau.getCaseParCoordonees(8,9).getJeton(),null);

    }

    @Test
    public void testChangementJouerApresDeplacementValide() throws RemoteException {

        jeu.setTour_courrant(j1);
        Jeton j = plateau.getCaseParCoordonees(1,3).getJeton();
        Case c = plateau.getCaseParCoordonees(2,4);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getTour_courrant().getCouleur(),j2.getCouleur());
    }

    @Test
    public void testChangementJouerApresDeplacementInvalide() throws RemoteException {

        jeu.setTour_courrant(j1);
        Jeton j = plateau.getCaseParCoordonees(1,3).getJeton();
        Case c = plateau.getCaseParCoordonees(2,2);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getTour_courrant().getCouleur(),j1.getCouleur());
    }


    @Test
    public void testPionBloquer() throws RemoteException {

        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);

        plateau.getCaseParCoordonees(0,0).setJeton(new Pion(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(1,1).setJeton(new Pion(j2.getCouleur(),2));

        plateau.getCaseParCoordonees(3,3).setJeton(new Pion(j1.getCouleur(),3));
        plateau.getCaseParCoordonees(4,4).setJeton(new Pion(j2.getCouleur(),4));

        Jeton j = plateau.getCaseParCoordonees(4,4).getJeton();
        Case c = plateau.getCaseParCoordonees(2,2);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(plateau.getCaseParJeton(j).getPosition(),c.getPosition());
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Bloquer);
        Assert.assertEquals(jeu.getTour_courrant().getCouleur(),j2.getCouleur());

    }

    @Test
    public void testDameBloquer() throws RemoteException {

        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);

        plateau.getCaseParCoordonees(0,0).setJeton(new Dame(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(1,1).setJeton(new Pion(j2.getCouleur(),2));

        plateau.getCaseParCoordonees(3,3).setJeton(new Pion(j1.getCouleur(),3));
        plateau.getCaseParCoordonees(4,4).setJeton(new Pion(j2.getCouleur(),4));

        Jeton j = plateau.getCaseParCoordonees(4,4).getJeton();
        Case c = plateau.getCaseParCoordonees(2,2);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(plateau.getCaseParJeton(j).getPosition(),c.getPosition());
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Bloquer);
        Assert.assertEquals(jeu.getTour_courrant().getCouleur(),j2.getCouleur());
    }

    @Test
    public void testFinDuJeuUnjeton() throws RemoteException {
        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);

        plateau.getCaseParCoordonees(3,3).setJeton(new Pion(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(4,4).setJeton(new Pion(j2.getCouleur(),2));

        Jeton j = plateau.getCaseParCoordonees(4,4).getJeton();
        Case c = plateau.getCaseParCoordonees(2,2);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(plateau.getCaseParJeton(j).getPosition(),c.getPosition());
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Terminer);
        Assert.assertEquals(jeu.getTour_courrant().getCouleur(),j2.getCouleur());
    }

    @Test
    public void testFinDuJeu25DeplacementSimple() throws RemoteException {
        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);
        jeu.setCompteur_deplacement_simple(24);

        plateau.getCaseParCoordonees(3,3).setJeton(new Pion(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(6,6).setJeton(new Pion(j2.getCouleur(),2));

        Jeton j = plateau.getCaseParCoordonees(6,6).getJeton();
        Case c = plateau.getCaseParCoordonees(5,5);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getCompteur_deplacement_simple(),25);
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Egalite);
    }

    @Test
    public void testFinDuJeu3Dame() throws RemoteException {
        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);
        jeu.setCompteur_deplacement_simple(15);

        plateau.getCaseParCoordonees(4,4).setJeton(new Dame(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(6,8).setJeton(new Dame(j2.getCouleur(),2));
        plateau.getCaseParCoordonees(0,7).setJeton(new Dame(j2.getCouleur(),3));

        Jeton j = plateau.getCaseParCoordonees(6,8).getJeton();
        Case c = plateau.getCaseParCoordonees(7,7);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getCompteur_deplacement_simple(),16);
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Egalite);
    }

    @Test
    public void testFinDuJeu2Dame1Pion() throws RemoteException {
        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);
        jeu.setCompteur_deplacement_simple(15);

        plateau.getCaseParCoordonees(4,4).setJeton(new Pion(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(6,8).setJeton(new Dame(j2.getCouleur(),2));
        plateau.getCaseParCoordonees(0,7).setJeton(new Dame(j2.getCouleur(),3));

        Jeton j = plateau.getCaseParCoordonees(6,8).getJeton();
        Case c = plateau.getCaseParCoordonees(7,7);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getCompteur_deplacement_simple(),16);
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Egalite);
    }

    @Test
    public void testFinDuJeu1Dame1Dame() throws RemoteException {
        plateau.supprimerTousLesPions();

        jeu.setTour_courrant(j2);
        jeu.setCompteur_deplacement_simple(15);

        plateau.getCaseParCoordonees(4,4).setJeton(new Dame(j1.getCouleur(),1));
        plateau.getCaseParCoordonees(6,8).setJeton(new Dame(j2.getCouleur(),2));

        Jeton j = plateau.getCaseParCoordonees(6,8).getJeton();
        Case c = plateau.getCaseParCoordonees(7,7);

        jeu.deplacerJeton(j,c);

        Assert.assertEquals(jeu.getCompteur_deplacement_simple(),16);
        Assert.assertEquals(jeu.getDeplacement(),Deplacement.Egalite);

    }



}

