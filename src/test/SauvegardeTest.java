/**
 * Classe de tests du package save
 */



import core.Case;
import core.Jeu;
import core.Joueur;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import save.JsonFile;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.ArrayList;

@RunWith(JUnitPlatform.class)
public class SauvegardeTest {


    private static Jeu jeuInitiale;
    private static Jeu jeuCharger;

    @Before
    public void Before() throws RemoteException{
        Joueur j1= new Joueur( "Jean", Color.red);
        Joueur j2= new Joueur( "Jacques", Color.green);
        jeuInitiale=new Jeu(j1,j2);
        jeuInitiale.initialisation();
        JsonFile json = new JsonFile();
        json.serialization(jeuInitiale);
        jeuCharger=json.deserialization();
    }


    @Test
    public void testJsonSaveJoueur() throws RemoteException {

            Assert.assertEquals(jeuCharger.getJoueur1().getNom(),jeuInitiale.getJoueur1().getNom());
            Assert.assertEquals(jeuCharger.getJoueur1().getCouleur(),jeuInitiale.getJoueur1().getCouleur());

            Assert.assertEquals(jeuCharger.getJoueur2().getNom(),jeuInitiale.getJoueur2().getNom());
            Assert.assertEquals(jeuCharger.getJoueur2().getCouleur(),jeuInitiale.getJoueur2().getCouleur());
    }

    @Test
    public void testJsonSavePlateau() throws RemoteException {

        ArrayList<Case> list_case_charger = jeuCharger.getPlateau().getListe_case();
        ArrayList<Case> list_case_init = jeuInitiale.getPlateau().getListe_case();

        Assert.assertEquals(list_case_charger.size(),list_case_init.size());

        for (int i=0;i<list_case_charger.size();i++){
            Assert.assertEquals(list_case_charger.get(i).getPosition(), list_case_init.get(i).getPosition());
            Assert.assertEquals(list_case_charger.get(i).getCouleur(), list_case_init.get(i).getCouleur());
            if(list_case_charger.get(i).getJeton() != null){
                Assert.assertEquals(list_case_charger.get(i).getJeton().getCouleur(), list_case_init.get(i).getJeton().getCouleur());
            }
        }
    }


}
