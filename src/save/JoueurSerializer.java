package save;

import core.Joueur;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Classe permettant de serialiser un joueur du jeu
 */
public class JoueurSerializer extends JsonSerializer<Joueur> {


    public JoueurSerializer(){
       // this(null);
        super();
    }



    @Override
    public void serialize(
            Joueur value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeStringField("nom", value.getNom());
        jgen.writeNumberField("color", value.getCouleur().getRGB());

        jgen.writeEndObject();
    }

}
