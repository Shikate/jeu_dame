package save;

import core.Case;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Classe permettant de serialiser la case du plateau
 */
public class CaseSerializer extends JsonSerializer<Case> {


    public CaseSerializer(){
       // this(null);
        super();
    }



    @Override
    public void serialize(
            Case value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeNumberField("x", value.getPosition().getX());
        jgen.writeNumberField("y", value.getPosition().getY());
        jgen.writeNumberField("color", value.getCouleur().getRGB());
        jgen.writeObjectField("jeton",value.getJeton());


        jgen.writeEndObject();
    }

}
