package save;



import com.google.common.base.Charsets;
import com.google.common.io.Files;
import core.Jeu;
import network.IJeu;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;


/**
 * Classe permettant d'effectuer les operation de serialization et deserialization json
 */
public class JsonFile {


    /**
     * Serialize un jeu donne
     * @param jeu le jeu a serializer
     */
    public void serialization(IJeu jeu){
        ObjectMapper mapper = new ObjectMapper();

        // Convert object to JSON string and save into a file directly
        mapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        //creation du fichier
        String nom_fichier = "jeu.json";
        File f = new File(nom_fichier);
        try {
            //on serialize l'objet dans le fichier
            mapper.writeValue(f, jeu);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Deserialize un jeu
     * @return le jeu deserialize contenu dans jeu.json
     */
    public Jeu deserialization() {

        ObjectMapper mapper = new ObjectMapper();
        Jeu jeu = null;
        try {

            File f = new File("jeu.json");
            //creation de l'objet avec lecture avec guava et precision sur la classe de l'objet a creer
            String fafa= Files.toString(new File(f.getPath()), Charsets.UTF_8);

            jeu = mapper.readValue(fafa,Jeu.class);

            //affichage de l'objet recrée avec le mapper

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch( Exception e){
            e.printStackTrace();
        }


        return jeu;

    }


}
