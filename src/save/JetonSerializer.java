package save;

import core.Dame;
import core.Jeton;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Classe permettant de serialiser le jeton du plateau
 */
public class JetonSerializer extends JsonSerializer<Jeton> {


    public JetonSerializer(){
       // this(null);
        super();
    }



    @Override
    public void serialize(
            Jeton value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeNumberField("color",value.getCouleur().getRGB());
        jgen.writeBooleanField("en_jeu",value.isEst_en_jeu());
        jgen.writeBooleanField("est_dame",value.getClass()== Dame.class);
        jgen.writeNumberField("id",value.getId());
        jgen.writeEndObject();
    }

}
