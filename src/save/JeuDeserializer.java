package save;

import core.*;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe permettant de deserialiser le jeu enregistre
 */
public class JeuDeserializer extends StdDeserializer<Jeu> {

    public JeuDeserializer(){
        this(null);
    }
    public JeuDeserializer(Class<?> vc){
        super(vc);
    }
    @Override
    public Jeu deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        //recuperation des joueurs
        //j1
        String nom=node.get("joueur1").get("nom").getTextValue();
        int couleur=node.get("joueur1").get("color").getIntValue();
        Joueur j1=new Joueur(nom,couleur);
        //j2
        nom=node.get("joueur2").get("nom").getTextValue();
        couleur=node.get("joueur2").get("color").getIntValue();
        Joueur j2=new Joueur(nom,couleur);

        //recuperation du plateau

        ArrayList<Case> ac= new ArrayList<Case>();
        for(JsonNode js : node.get("plateau").get("liste_case")){
            Case c =new Case(new Point(js.get("x").getIntValue(),js.get("y").getIntValue()),new Color(js.get("color").getIntValue()));
            if(js.get("jeton").toString()!="null"){
                //si c'est un pion
                Jeton j;
                if(js.get("jeton").get("est_dame").asBoolean()){
                    j=new Dame(js.get("jeton").get("color").getIntValue(),js.get("jeton").get("id").getIntValue());
                }
                //sinon
                else{
                    j=new Pion(js.get("jeton").get("color").getIntValue(),js.get("jeton").get("id").getIntValue());
                }
                j.setEst_en_jeu(js.get("jeton").get("en_jeu").getBooleanValue());
                c.setJeton(j);
            }
            ac.add(c);


        }
        //Determination du tour
        Joueur j_courant;
        if(node.get("tour_courrant").get("color").getIntValue() == j1.getCouleur().getRGB()){
            j_courant=j1;
        }else{
            j_courant=j2;
        }
        Jeu j=new Jeu(j1,j2,ac,j_courant);

        j.setCompteur_deplacement_simple(node.get("compteur_deplacement_simple").getIntValue());

        return j;
    }


}
