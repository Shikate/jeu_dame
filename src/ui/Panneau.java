package ui;

import core.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import network.Client;
import network.IJeu;
import save.JsonFile;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Panneau extends Application {

    /**
     * Attribut nombre de case un longeur
     */
    public static int WIDTH=10;

    /**
     * Attribut nombre de case en hauteur
     */
    public static int HEIGT=10;

    /**
     * Attribut taille d'une case
     */
    public static int CASE_SIZE=50;

    /**
     * Attribut Jeu
     */
    private IJeu jeu;

    /**
     * Attribut jeton en cours de selection
     */
    private Case casePionSelect;

    private Label valueJoueurEnCours;

    private ArrayList<Case> liste_case;
    private Group caseGroup = new Group();

    private Stage primaryStage;

    private Client client;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;

        primaryStage.setTitle("Jeu de Dame");
        genererChoixModeJeu();
    }

    /**
     * generation de la scene de choix du mode de jeu
     */
    private void genererChoixModeJeu(){
        Pane root = new Pane();

        GridPane grid = new GridPane();

        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);

        root.getChildren().add(grid);

        Button buttonNouvellePartie = new Button("Nouvelle partie Local");
        GridPane.setConstraints(buttonNouvellePartie, 0, 0);
        grid.getChildren().add(buttonNouvellePartie);

        buttonNouvellePartie.setOnMousePressed( e -> {
            //creation d'une partie local
            Joueur j1= new Joueur( "Joueur 1", java.awt.Color.BLUE);
            Joueur j2= new Joueur( "Joueur 2", java.awt.Color.GREEN);
            try {
                jeu=new Jeu(j1,j2);
                jeu.initialisation();
                liste_case=jeu.getPlateau().getListe_case();
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
            genererPlateau();
        });


        Button buttonChargerPartie = new Button("Charger une partie Local");
        GridPane.setConstraints(buttonChargerPartie, 1, 0);
        grid.getChildren().add(buttonChargerPartie);

        buttonChargerPartie.setOnMousePressed( e-> {
            //chargement d'une partie
            JsonFile jf = new JsonFile();
            this.jeu =jf.deserialization();
            try {
                liste_case=jeu.getPlateau().getListe_case();
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
            genererPlateau();
        });

        Button buttonNouvellePartieR = new Button("Nouvelle partie Réseaux");
        GridPane.setConstraints(buttonNouvellePartieR, 2, 0);
        grid.getChildren().add(buttonNouvellePartieR);

        buttonNouvellePartieR.setOnMousePressed( e -> {
            genererConnexion();
        });

        this.primaryStage.setScene( new Scene(root) );
        primaryStage.show();
    }

    /**
     * generation du plateau de jeu
     */
    private void genererPlateau() {
        BorderPane root = new BorderPane();

        root.setPrefSize(WIDTH*CASE_SIZE,HEIGT*CASE_SIZE+50);
        root.setTop(caseGroup);

        caseGroup.getChildren().clear();
        caseGroup.getChildren().addAll(liste_case);
        for(Case c: liste_case){
            c.dessinerCase();
            c.setOnMousePressed(e -> selectCase((Case) e.getSource()));
        }

        Group controle = new Group();
        root.setCenter(controle);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);
        controle.getChildren().add(grid);

        Button buttonSauvegarder = new Button("Sauvegarder la partie");
        GridPane.setConstraints(buttonSauvegarder, 0, 0);
        grid.getChildren().add(buttonSauvegarder);

        buttonSauvegarder.setOnMousePressed(e ->{
            JsonFile js = new JsonFile();
            js.serialization(jeu);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sauvegarde");
            alert.setHeaderText("Réussie !");
            alert.setContentText("La sauvegarde de votre partie a réussie");
            alert.showAndWait();
        });

        Label labelJoueurEnCours = new Label("Au tour de : ");
        GridPane.setConstraints(labelJoueurEnCours,1,0);
        labelJoueurEnCours.setFont(Font.font ("Verdana", 18));
        grid.getChildren().add(labelJoueurEnCours);


        this.valueJoueurEnCours = new Label();
        genererValueJoueurEnCours();
        GridPane.setConstraints(valueJoueurEnCours,2,0);
        valueJoueurEnCours.setFont(Font.font ("Verdana", 18));
        grid.getChildren().add(valueJoueurEnCours);

        this.primaryStage.setScene( new Scene(root) );
        primaryStage.show();
    }

    /**
     * generation de valeur dans le champs ValueJoueurEnCours
     */
    private void genererValueJoueurEnCours() {
        try {
            this.valueJoueurEnCours.setText(this.jeu.getTour_courrant().getNom());
            this.valueJoueurEnCours.setTextFill(javafx.scene.paint.Color.rgb(this.jeu.getTour_courrant().getCouleur().getRed(),this.jeu.getTour_courrant().getCouleur().getGreen(),this.jeu.getTour_courrant().getCouleur().getBlue()));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * generation de l'interface de connexion
     */
    private void genererConnexion(){
        Pane root = new Pane();

        GridPane grid = new GridPane();

        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);

        root.getChildren().add(grid);


        Label labelNomJoueur = new Label("Pseudo:");
        GridPane.setConstraints(labelNomJoueur, 0, 0);
        grid.getChildren().add(labelNomJoueur);

        TextField champNomJoueur = new TextField();
        champNomJoueur.setPromptText("ex: toto");
        champNomJoueur.setPrefColumnCount(10);
        GridPane.setConstraints(champNomJoueur, 1, 0);
        grid.getChildren().add(champNomJoueur);

        Label labelIpServeur = new Label("Ip:");
        GridPane.setConstraints(labelIpServeur, 0, 1);
        grid.getChildren().add(labelIpServeur);

        TextField champIpServeur = new TextField();
        champIpServeur.setPromptText("ex: 127.0.0.1");
        champIpServeur.setPrefColumnCount(10);
        GridPane.setConstraints(champIpServeur, 1, 1);
        grid.getChildren().add(champIpServeur);

        Button submit = new Button("Connexion");
        GridPane.setConstraints(submit, 2, 0);
        grid.getChildren().add(submit);

        submit.setOnAction(event -> {
            if(champNomJoueur.getText()!=null && champIpServeur.getText() != null){
                try {
                    this.client = new Client();
                    this.jeu= this.client.connexionJoueur(champIpServeur.getText(),champNomJoueur.getText());

                    if(this.jeu.getJoueur2() == null){
                        genererAttenteJoueur();
                        while (this.jeu.getJoueur2() == null){
                            try {
                                sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    liste_case=this.jeu.getPlateau().getListe_case();
                    this.genererPlateau();

                    if(client.getJoueur().getCouleur().equals(this.jeu.getJoueur2().getCouleur())){
                        while(!client.getJoueur().getCouleur().equals(jeu.getTour_courrant().getCouleur())){
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        liste_case=this.jeu.getPlateau().getListe_case();
                        genererPlateau();
                    }

                } catch (RemoteException | MalformedURLException | NotBoundException e) {

                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Erreur de connexion");
                    alert.setHeaderText("Une erreur est survenue durant la connexion au serveur");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }
        });

        this.primaryStage.setScene( new Scene(root) );
        primaryStage.show();
    }

    /**
     * generation panel attente de connexion d'un joueur
     */
    private void genererAttenteJoueur(){
        Pane root = new Pane();

        Label labelAttente = new Label("attente d'un deuxieme joueur...");
        root.getChildren().addAll(labelAttente);

        this.primaryStage.setScene( new Scene(root) );
        primaryStage.show();
    }


    /**
     * generer l'interface de fin
     * @param d Deplacement
     */
    private void genererFin(Deplacement d){
        Pane root = new Pane();

        GridPane grid = new GridPane();

        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);

        root.getChildren().add(grid);

        String resultat = null;

        if(d==Deplacement.Terminer || d==Deplacement.Bloquer){
            try {
                resultat="Victoire de "+jeu.getTour_courrant().getNom();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        else{
            resultat="Egalité !";
        }

        Label labelResultat = new Label(resultat);
        labelResultat.setFont(Font.font ("Verdana", 20));
        labelResultat.setTextFill(javafx.scene.paint.Color.GREEN);
        GridPane.setConstraints(labelResultat, 0, 0);
        grid.getChildren().add(labelResultat);


        this.primaryStage.setScene( new Scene(root) );
        primaryStage.show();
    }

    /**
     * Fonction de selection d'une case
     * @param c
     */
    private void selectCase(Case c){
        try {
            if(!c.estVide() && jeu.getTour_courrant().estMonJeton(c.getJeton()) && (client==null || client.getJoueur().getCouleur().equals(jeu.getTour_courrant().getCouleur())) ){
                if(casePionSelect!=null) {
                    casePionSelect.unSelectCase();
                }
                c.selectCase();
                casePionSelect=c;
            }else{
                if(casePionSelect!=null && c.estVide()){
                    c.selectCase();
                    this.jeu.deplacerJeton(casePionSelect.getJeton(), c);

                    if(client!=null){
                        liste_case=this.jeu.getPlateau().getListe_case();
                        genererPlateau();
                        System.out.println("test2");
                    }
                    else{
                        casePionSelect.unSelectCase();
                        c.unSelectCase();
                    }
                    casePionSelect=null;
                    genererValueJoueurEnCours();

                    if( this.jeu.getDeplacement() == Deplacement.Bloquer || this.jeu.getDeplacement() == Deplacement.Terminer || this.jeu.getDeplacement() == Deplacement.Egalite ){
                        genererFin(this.jeu.getDeplacement());
                    }
                    if(client!=null){
                        /*while(!client.getJoueur().getCouleur().equals(jeu.getTour_courrant().getCouleur())){
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        liste_case=this.jeu.getPlateau().getListe_case();
                        genererPlateau();*/

                    }
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
